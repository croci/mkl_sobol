#!/usr/bin/env python3

#######################################################################
#
# This file is part of mkl_sobol
#
# mkl_sobol is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

from setuptools import setup, find_packages
# on the MI machines better to install with python3 setup.py develop --prefix=/home/croci/install-scripts/dolfin/fenics-dev-20180319
from setuptools.command.install import install
import subprocess
import glob
import os

def compile_and_install_software():
    """Used the subprocess module to compile/install the C software."""
    src_path = './mkl_sobol/'

    # compile the software
    subprocess.check_call("make", cwd=src_path, shell=True)

class CustomInstall(install):
    """Custom handler for the 'install' command."""
    def run(self):
        compile_and_install_software()
        super().run()

dir_path = os.path.dirname(os.path.realpath(__file__))
scripts = [file for file in glob.glob(dir_path + "/*.py")] + [file for file in glob.glob(dir_path + "/*.so")]

setup(
    name="mkl_sobol",
    version="0.1",
    packages=["mkl_sobol"],
    package_dir={"mkl_sobol" : "mkl_sobol"},
    package_data={"mkl_sobol" : ["libmkl_sobol.so", "mkl_sobol.dat"]},
    cmdclass={'install': CustomInstall},
)

