# mkl_sobol

A C and Python wrapper for some of the pseudo-random and Sobol point generation routines in the MKL library that supports Sobol point generation and digital scrambling up to 21201 dimensions.

## Requirements

* Python3
* Intel MKL library (version >= 2018.0)
* Intel ICC compiler (recommended) (version >= 18.0)

## Getting started

Edit the Makefile to compile with GCC in case ICC is not installed on your machine.
You might first need to: 

1. Add the Intel libraries to your ```LD_LIBRARY_PATH```.
2. Define the ```MKLROOT``` environment variable.
3. Run ```source /path/to/mklvars.sh```. 

You might find useful to add the above commands to your ```.bash_profile``` or ```.bashrc```.

* Installation Instructions 1: ```cd mkl_sobol``` then run ```make```. Test with ```make test```. Uninstall with ```make clean```, then set your PYTHONPATH accordingly.
* Installation Instructions 2: just run ```python3 setup.py install```.

To use the Python interface, use
```
#!python3
from mkl_sobol import MKL_RNG, MKL_SOBOL_RNG
```
in your Python file header.

To learn how to use the interface, see the main in file ```mkl_sobol.py```.

## License
* mkl_sobol is available under the [GNU Lesser General Public License version 3](http://www.gnu.org/licenses/lgpl.html).

## Acknowledgements
* The Sobol sequence generating polynomials and direction numbers are taken from [Frances Kuo's webiste](http://web.maths.unsw.edu.au/~fkuo/sobol/). 

## Authors
* Matteo Croci, University of Oxford, [matteo.croci@maths.ox.ac.uk](mailto:matteo.croci@maths.ox.ac.uk)
