#!/usr/bin/env python3

#######################################################################
#
# This file is part of mkl_sobol
#
# mkl_sobol is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

import ctypes
from ctypes import RTLD_GLOBAL
import numpy as np
import sys
import os
import atexit

#FIXME: Leapfrog has been interfaced, but I would not know how to use it with the Sobol Python class. It does not work for MKL_RNG as it is not supported by Intel

## Load shared library and prescribe the input types of each function in the library
#_sobol = ctypes.CDLL("libmkl_sobol.so", 0x100 | 0x2)
_sobol = ctypes.CDLL(os.path.dirname(os.path.realpath(__file__)) + "/libmkl_sobol.so", RTLD_GLOBAL)

_sobol.DeallocateStreamPtr.argtypes = (ctypes.POINTER(ctypes.c_void_p),)

_sobol.SetupRandomStream.argtypes = (ctypes.POINTER(ctypes.c_void_p), ctypes.c_uint32)
_sobol.SetupSobolStream.argtypes = (ctypes.POINTER(ctypes.c_void_p), ctypes.c_uint32)

_sobol.mkl_sobol_rand.argtypes = (ctypes.c_uint32, ctypes.c_uint32, ctypes.c_void_p, ctypes.POINTER(ctypes.c_uint32), ctypes.POINTER(ctypes.c_double))
_sobol.mkl_sobol_randn.argtypes = (ctypes.c_uint32, ctypes.c_uint32, ctypes.c_void_p, ctypes.POINTER(ctypes.c_uint32), ctypes.POINTER(ctypes.c_double))
_sobol.mkl_sobol_rand_bits.argtypes = (ctypes.c_uint32, ctypes.c_uint32, ctypes.c_void_p, ctypes.POINTER(ctypes.c_uint32), ctypes.POINTER(ctypes.c_uint32))
_sobol.mkl_rand.argtypes = (ctypes.c_uint32, ctypes.c_void_p, ctypes.POINTER(ctypes.c_double))
_sobol.mkl_randn.argtypes = (ctypes.c_uint32, ctypes.c_void_p, ctypes.POINTER(ctypes.c_double))
_sobol.mkl_rand_bits.argtypes = (ctypes.c_uint32, ctypes.c_void_p, ctypes.POINTER(ctypes.c_uint32))

_sobol.SkipAheadSobolStream.argtypes = (ctypes.c_void_p, ctypes.c_uint32, ctypes.c_uint32)
_sobol.LeapfrogSobolStream.argtypes = (ctypes.c_void_p, ctypes.c_uint32)
_sobol.SkipAheadStream.argtypes = (ctypes.c_void_p, ctypes.c_longlong)
_sobol.LeapfrogStream.argtypes = (ctypes.c_void_p, ctypes.c_int, ctypes.c_int)

_sobol.CopyStreamState.argtypes = (ctypes.c_void_p, ctypes.c_void_p)

int_types = (int, np.int, np.int64, np.int32)

class MKL_RNG(object):
    # pseudo-random point generator

    def __init__(self, seed=1):

        if seed < 0 or not isinstance(seed, int_types):
            raise ValueError("Seed must be a non-negative integer")

        self._seed = seed

        stream = ctypes.c_void_p(None)
        _sobol.SetupRandomStream(ctypes.byref(stream), ctypes.c_uint32(seed))
        self._stream = stream

        self.nskip = 0

        atexit.register(self.__deallocate_stream)

    def __deallocate_stream(self):
        _sobol.DeallocateStreamPtr(ctypes.byref(self._stream))
        #del self._seed
        #del self.nskip

    def copy(self):
        out = MKL_RNG(seed = self._seed)
        out.nskip = self.nskip
        _sobol.CopyStreamState(out._stream, self._stream)
        return out

    def skipahead(self, nskip):
        # nskip is the number of points to skip
        _sobol.SkipAheadStream(self._stream, ctypes.c_longlong(nskip))
        self.nskip += nskip

    def reset(self, seed=None):
        # reinitialises the generator from scratch with a new seed
        if seed is None:
            seed = self._seed

        if seed < 0 or not isinstance(seed, int_types):
            raise ValueError("Seed must be a non-negative integer")

        self._seed = seed
        _sobol.DeallocateStreamPtr(ctypes.byref(self._stream))

        stream = ctypes.c_void_p(None)
        _sobol.SetupRandomStream(ctypes.byref(stream), ctypes.c_uint32(seed))
        self._stream = stream

    def rand(self, *args):

        l = len(args)

        if not all(isinstance(a, int_types) for a in args):
            raise ValueError("Positive integer dimensions must be provided")

        total_dim = int(np.prod(args))

        if total_dim < 1:
            raise ValueError("Positive integer dimensions must be provided")

        out = np.empty((total_dim,))

        _sobol.mkl_rand(ctypes.c_uint32(total_dim), self._stream, out.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))

        if l > 0:
            return out.reshape(args)
        else:
            return out[0]

    def randn(self, *args):

        l = len(args)

        if not all(isinstance(a, int_types) for a in args):
            raise ValueError("Positive integer dimensions must be provided")

        total_dim = int(np.prod(args))

        if total_dim < 1:
            raise ValueError("Positive integer dimensions must be provided")

        out = np.empty((total_dim,))

        _sobol.mkl_randn(ctypes.c_uint32(total_dim), self._stream, out.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))

        if l > 0:
            return out.reshape(args)
        else:
            return out[0]

    def rand_bits(self, *args):
        # generates uniform pseudo-random points as uint32

        l = len(args)

        if not all(isinstance(a, int_types) for a in args):
            raise ValueError("Positive integer dimensions must be provided")

        total_dim = int(np.prod(args))

        if total_dim < 1:
            raise ValueError("Positive integer dimensions must be provided")

        out = np.empty((total_dim,), dtype=np.uint32)

        _sobol.mkl_rand_bits(ctypes.c_uint32(total_dim), self._stream, out.ctypes.data_as(ctypes.POINTER(ctypes.c_uint32)))

        if l > 0:
            return out.reshape(args)
        else:
            return out[0]


class MKL_SOBOL_RNG(object):
    def __init__(self, dim=1, randomized=True, seed=1):

        if not isinstance(dim, int_types) or dim < 0 or dim > 21201:
            raise ValueError("The dimension must be a positive integer smaller or equal than 21201")

        if not isinstance(seed, int_types) or seed < 0:
            raise ValueError("Seed must be a non-negative integer")

        self._seed = seed
        self.dim = dim
        self.randomized = randomized
        self.nskip = 0

        self.mkl_rng = None
        self.shifts = None

        if self.randomized == True:
            self.randomize()

        stream = ctypes.c_void_p(None)
        _sobol.SetupSobolStream(ctypes.byref(stream), ctypes.c_uint32(self.dim))
        self._stream = stream

        atexit.register(self.__deallocate_stream)

    def __deallocate_stream(self):
        _sobol.DeallocateStreamPtr(ctypes.byref(self._stream))
        #del self.shifts
        #del self._seed
        #del self.dim
        #del self.randomized
        #del self.nskip

    def copy(self):
        out = MKL_SOBOL_RNG(dim = self.dim, randomized = self.randomized, seed = self._seed)
        out.nskip = self.nskip
        out.shifts = self.shifts.copy()
        out.mkl_rng = self.mkl_rng.copy()
        _sobol.CopyStreamState(out._stream, self._stream)
        return out

    def skipahead(self, nskip):
        # NOTE: nskip here is the number of Sobol points to skip
        _sobol.SkipAheadSobolStream(self._stream, ctypes.c_uint32(self.dim), ctypes.c_uint32(nskip))
        self.nskip += nskip

    def skipahead_shift(self, nskip):
        # performs skipahead on the shift generator
        # NOTE: it does not skip from zero as the first N = self.dim points of self.mkl_rng have already been generated

        if not isinstance(nskip, int_types) or nskip < 0:
            raise ValueError("nskips must be a positive integer")

        if nskip == 0:
            return

        if self.randomized == False:
            raise RuntimeError("Cannot skipahead the shift generator if the Sobol sequence is not randomized")
        
        self.mkl_rng.skipahead(nskip)
        self.randomize()

    def randomize(self):
        # sets the digital scrambling shifts up and enables the call to the randomized sequence generator routine in C
        # it can also be used to randomized the sequence with a new set of shifts

        if self.randomized == False:
            self.randomized = True
            self.mkl_rng = MKL_RNG(seed=self._seed)
        if self.randomized == True and self.mkl_rng is None:
            self.mkl_rng = MKL_RNG(seed=self._seed)
            
        self.shifts = self.mkl_rng.rand_bits(self.dim)

    def reset(self, dim=None, randomized = None, seed = None):
        # Resets the generator from scratch
        
        if dim is None:
            dim = self.dim

        if randomized is None:
            randomized = self.randomized

        if seed is None:
            seed = self._seed

        if not isinstance(dim, int_types) or dim < 0 or dim > 21201:
            raise ValueError("The dimension must be a positive integer")

        if not isinstance(seed, int_types) or seed < 0:
            raise ValueError("Seed must be a non-negative integer")

        if self.randomized == True and randomized == False:
            del self.mkl_rng
            self.mkl_rng = None
            del self.shifts
            self.shifts = None
        elif self.randomized == True:
            self.mkl_rng.reset(seed = seed)

        self._seed = seed
        self.dim = dim
        self.randomized = randomized

        if self.randomized == True:
            self.randomize()

        _sobol.DeallocateStreamPtr(ctypes.byref(self._stream))

        stream = ctypes.c_void_p(None)
        _sobol.SetupSobolStream(ctypes.byref(stream), ctypes.c_uint32(self.dim))
        self._stream = stream

    def rand(self, M=None):

        noinput = False
        if M is None:
            M = 1
            noinput = True

        if not isinstance(M, int_types) or M < 1:
            raise ValueError("A positive integer number of points must be provided")

        out = np.empty((self.dim*M,))

        if self.randomized == True:
            _sobol.mkl_sobol_rand(ctypes.c_uint32(self.dim), ctypes.c_uint32(M), self._stream, self.shifts.ctypes.data_as(ctypes.POINTER(ctypes.c_uint32)), out.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))
        else:
            _sobol.mkl_rand(ctypes.c_uint32(self.dim*M), self._stream, out.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))

        if self.dim == 1 and noinput == True:
            return out[0]
        elif self.dim == 1 or M == 1:
            return out
        else:
            return out.reshape((M, self.dim))

    def randn(self, M=None):

        noinput = False
        if M is None:
            M = 1
            noinput = True

        if not isinstance(M, int_types) or M < 1:
            raise ValueError("A positive integer number of points must be provided")

        out = np.empty((self.dim*M,))

        if self.randomized == True:
            _sobol.mkl_sobol_randn(ctypes.c_uint32(self.dim), ctypes.c_uint32(M), self._stream, self.shifts.ctypes.data_as(ctypes.POINTER(ctypes.c_uint32)), out.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))
        else:
            _sobol.mkl_randn(ctypes.c_uint32(self.dim*M), self._stream, out.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))

        if self.dim == 1 and noinput == True:
            return out[0]
        elif self.dim == 1 or M == 1:
            return out
        else:
            return out.reshape((M, self.dim))

    def rand_bits(self, M=None):
        # generates uniform Sobol points as uint32

        noinput = False
        if M is None:
            M = 1
            noinput = True

        if not isinstance(M, int_types) or M < 1:
            raise ValueError("A positive integer number of points must be provided")

        out = np.empty((self.dim*M,), dtype=np.uint32)

        if self.randomized == True:
            _sobol.mkl_sobol_rand_bits(ctypes.c_uint32(self.dim), ctypes.c_uint32(M), self._stream, self.shifts.ctypes.data_as(ctypes.POINTER(ctypes.c_uint32)), out.ctypes.data_as(ctypes.POINTER(ctypes.c_uint32)))
        else:
            _sobol.mkl_rand_bits(ctypes.c_uint32(self.dim*M), self._stream, out.ctypes.data_as(ctypes.POINTER(ctypes.c_uint32)))

        if self.dim == 1 and noinput == True:
            return out[0]
        elif self.dim == 1 or M == 1:
            return out
        else:
            return out.reshape((M, self.dim))

if __name__ == "__main__":
    dim = 5
    dim2 = 124
    seed = 123456789

    print("Testing the pseudorandom generator...\n")
    rng = MKL_RNG(seed)

    a = rng.rand()
    b = rng.rand(5,2)
    c = rng.randn(5,2)
    d = rng.rand_bits(5,2)

    rng.reset()
    aha = rng.copy()
    #print("\nThese should be the same\n")
    assert np.allclose(a, rng.rand())
    assert np.allclose(b, rng.rand(5,2))
    assert np.allclose(c, rng.randn(5,2))
    assert np.allclose(d, rng.rand_bits(5,2))

    #print("\nThese should be the same\n")
    assert np.allclose(a, aha.rand())
    assert np.allclose(b, aha.rand(5,2))
    assert np.allclose(c, aha.randn(5,2))
    assert np.allclose(d, aha.rand_bits(5,2))

    aha.reset()
    aha.skipahead(1000)
    #print("\nThese should be different\n")
    assert not np.allclose(a, aha.rand())
    assert not np.allclose(b, aha.rand(5,2))
    assert not np.allclose(c, aha.randn(5,2))
    assert not np.allclose(d, aha.rand_bits(5,2))

    rng.reset(seed = seed + 1)
    #print("\nThese should be different\n")
    assert not np.allclose(a, rng.rand())
    assert not np.allclose(b, rng.rand(5,2))
    assert not np.allclose(c, rng.randn(5,2))
    assert not np.allclose(d, rng.rand_bits(5,2))

    print("Testing the sobol sequence generator:\n")
    print("Without randomization...\n")
    rng = MKL_SOBOL_RNG(dim=dim, randomized = False, seed = seed)

    a = rng.rand()
    b = rng.rand(4)
    c = rng.randn(4)
    d = rng.rand_bits(4)

    rng.reset()
    #print("\nThese should be the same\n")
    assert np.allclose(a, rng.rand())
    assert np.allclose(b, rng.rand(4))
    assert np.allclose(c, rng.randn(4))
    assert np.allclose(d, rng.rand_bits(4))

    rng.reset(seed = seed + 1)
    #print("\nThese should be the same again as we are not scrambling\n")
    assert np.allclose(a, rng.rand())
    assert np.allclose(b, rng.rand(4))
    assert np.allclose(c, rng.randn(4))
    assert np.allclose(d, rng.rand_bits(4))

    print("With randomization...\n")
    rng.reset(dim = dim, seed=seed, randomized = True)

    a = rng.rand()
    b = rng.rand(4)
    c = rng.randn(4)
    d = rng.rand_bits(4)

    rng.reset()
    aha = rng.copy()
    #print("\nThese should be the same\n")
    assert np.allclose(a, rng.rand())
    assert np.allclose(b, rng.rand(4))
    assert np.allclose(c, rng.randn(4))
    assert np.allclose(d, rng.rand_bits(4))

    #print("\nThese should be the same\n")
    assert np.allclose(a, aha.rand())
    assert np.allclose(b, aha.rand(4))
    assert np.allclose(c, aha.randn(4))
    assert np.allclose(d, aha.rand_bits(4))

    aha.reset()
    aha.skipahead(1000)
    #print("\nThese should be different\n")
    assert not np.allclose(a, aha.rand())
    assert not np.allclose(b, aha.rand(4))
    assert not np.allclose(c, aha.randn(4))
    assert not np.allclose(d, aha.rand_bits(4))

    rng.reset(seed = seed + 1)
    #print("\nThese should be different\n")
    assert not np.allclose(a, rng.rand())
    assert not np.allclose(b, rng.rand(4))
    assert not np.allclose(c, rng.randn(4))
    assert not np.allclose(d, rng.rand_bits(4))

    rng.reset(dim = dim2)
    #print("\nThese should have a different dimension\n")
    assert a.shape != rng.rand().shape
    assert b.shape != rng.rand(4).shape
    assert c.shape != rng.randn(4).shape
    assert d.shape != rng.rand_bits(4).shape

    print("TEST PASSED!")
