/*
#######################################################################
#
# This file is part of mkl_sobol
#
# mkl_sobol is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <omp.h>

#include <mkl.h>


// Number of Sobol points to generate
#define M   (1024*1024)

#define MAX_DIM 21201

#define BLOCK_SIZE 1
#define N_BLOCKS (M/BLOCK_SIZE)

#define TWO_POWER_MINUS_32 0x1p-32f

#define ALIGN 64

// Routine to check memory allocation status
void check_allocation_successful(void *buf);

// Routine to read Sobol sequence data from file
int load_sobol_file(int n_sobol_poly, int maxdeg, unsigned int **dir_numbers, unsigned int *primitive_polys);

// Routine to integrate multidimensional function using QMC
void compute_integral(int dim, int n, double *r, double *partial1, double *partial2 );

// Routine to deallocate streams given a stream pointer
void DeallocateStreamPtr(VSLStreamStatePtr ** streamptr);

// Routine to allocate streams given a stream pointer
void SetupRandomStream(VSLStreamStatePtr ** streamptr, const unsigned int seed);
void SetupSobolStream(VSLStreamStatePtr ** streamptr, const unsigned int dim);

// Routines for Sobol quasi-random number generation
void mkl_sobol_rand(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, const unsigned int * restrict shifts, double * restrict out);
void mkl_sobol_randn(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, const unsigned int * restrict shifts, double * restrict out);
void mkl_sobol_rand_bits(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, const unsigned int * restrict shifts, unsigned int * restrict out);

// Routines for standard pseudo-random number generation
void mkl_rand_bits(const unsigned int dim, const VSLStreamStatePtr * streamptr, unsigned int * restrict out);
void mkl_randn(const unsigned int dim, const VSLStreamStatePtr * streamptr, double * restrict out);
void mkl_rand(const unsigned int dim, const VSLStreamStatePtr * streamptr, double * restrict out);

// Routines for skipahead and leapfrog of random streams
void SkipAheadSobolStream(const VSLStreamStatePtr * streamptr, const unsigned int dim, const unsigned int npoints_skip);
void LeapfrogSobolStream(const VSLStreamStatePtr * streamptr, const unsigned int component);
void SkipAheadStream(const VSLStreamStatePtr * streamptr, const long long int nskip);
void LeapfrogStream(const VSLStreamStatePtr * streamptr, const int k, const int nstreams);

// Routines for copying stream states
void CopyStreamState(const VSLStreamStatePtr * deststreamptr, const VSLStreamStatePtr * srcstreamptr);

int main(int argc, char *argv[]){
    VSLStreamStatePtr *streamptr, *streamptr2;

    double * restrict r __attribute__((aligned(ALIGN)));
    unsigned int * restrict shifts __attribute__((aligned(ALIGN)));

    double error, sum = 0.0, sum2 = 0.0, partial1, partial2;
    int status = 0;
    unsigned int i;

    const unsigned int dim = 1024;
    const unsigned int seed = 123456789;

    const unsigned int nb = dim*BLOCK_SIZE;

    // Allocate memory and initialise streams
    r = (double *)mkl_malloc(nb*sizeof(double), ALIGN);
    check_allocation_successful(r);

    shifts = (unsigned int *)mkl_malloc(dim*sizeof(unsigned int), ALIGN);
    check_allocation_successful(shifts);

    SetupSobolStream(&streamptr, dim);
    SetupRandomStream(&streamptr2, seed);

    mkl_rand_bits(dim, streamptr2, shifts);
    for(i = 0; i<N_BLOCKS; i++){

        mkl_sobol_rand(dim, BLOCK_SIZE, streamptr, shifts, r);
        compute_integral(dim, BLOCK_SIZE, r, &partial1, &partial2 );

        sum += partial1;
        sum2 += partial2;
    }

    sum /= (double)(M);
    sum2 /= (double)(M);

    error = sqrt((sum2 - sum*sum)/(double)(M));

    printf("Computed %d dimensional integral.\n", dim);
    printf("Approximated value: %.15f, Exact: 1.0\n", sum);
    printf("sigma/sqrt(N)  : %e, absolute error: %e\n", error, fabs(1-sum));

    // Deallocate
    DeallocateStreamPtr(&streamptr);
    DeallocateStreamPtr(&streamptr2);

    mkl_free(r);
    mkl_free(shifts);

    if (fabs(1-sum) > 3*error){
        printf("\nTEST FAILED!\n");
        return 1;
    }

    printf("\nTEST PASSED!\n");
    return 0;
}


void DeallocateStreamPtr(VSLStreamStatePtr ** streamptr){
    int status = 0;
    status = vslDeleteStream(*streamptr);
    mkl_free(*streamptr);
}

void CopyStreamState(const VSLStreamStatePtr * deststreamptr, const VSLStreamStatePtr * srcstreamptr){
    int status = 0;
    status = vslCopyStreamState(*deststreamptr, *srcstreamptr);
}

//NOTE: the following skipsahead the points in the sequence. It is also possible to skip ahead single components
//      but I do not know how whether this would work correctly with the Python side
void SkipAheadSobolStream(const VSLStreamStatePtr * streamptr, const unsigned int dim, const unsigned int npoints_skip){
    int status = 0;
    const long long int nskip = (long long int) dim*npoints_skip;
    status = vslSkipAheadStream( *streamptr, nskip);
}

void LeapfrogSobolStream(const VSLStreamStatePtr * streamptr, const unsigned int component){
    int status = 0;
    status =  vslLeapfrogStream( *streamptr, (MKL_INT) component, VSL_QRNG_LEAPFROG_COMPONENTS );
}

void SkipAheadStream(const VSLStreamStatePtr * streamptr, const long long int nskip){
    int status = 0;
    status = vslSkipAheadStream( *streamptr, nskip);
}

void LeapfrogStream(const VSLStreamStatePtr * streamptr, const int k, const int nstreams){
    int status = 0;
    status =  vslLeapfrogStream( *streamptr, (MKL_INT) k, (MKL_INT) nstreams );
}

void SetupRandomStream(VSLStreamStatePtr ** streamptr, const unsigned int seed){
    int status = 0;
    const MKL_UINT mkl_seed = (MKL_UINT) seed;
    // Remember to free this later
    *streamptr = (VSLStreamStatePtr*) mkl_malloc(sizeof(VSLStreamStatePtr), ALIGN);
    check_allocation_successful(streamptr);
    status = vslNewStream( *streamptr, VSL_BRNG_SFMT19937, mkl_seed);
}

void mkl_randn(const unsigned int dim, const VSLStreamStatePtr * streamptr, double * restrict out){
    #ifdef __INTEL_COMPILER
        __assume_aligned(out, ALIGN);
    #endif
    int status = 0;
    status = vdRngGaussian( VSL_RNG_METHOD_GAUSSIAN_ICDF, *streamptr, dim, out, 0, 1);
}

void mkl_rand(const unsigned int dim, const VSLStreamStatePtr * streamptr, double * restrict out){
    int status = 0;
    #ifdef __INTEL_COMPILER
        __assume_aligned(out, ALIGN);
    #endif
    status = vdRngUniform( VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, *streamptr, dim, out, 0, 1);
}

void mkl_rand_bits(const unsigned int dim, const VSLStreamStatePtr * streamptr, unsigned int * restrict out){
    int status = 0;
    #ifdef __INTEL_COMPILER
        __assume_aligned(out, ALIGN);
    #endif
    status = viRngUniformBits( VSL_RNG_METHOD_UNIFORMBITS_STD, *streamptr, dim, out);
}

////NOTE: can do the following directly in Python
//void mkl_sobol_rand_unscrambled(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, double * restrict out)
//{
//    const unsigned int nb = m*dim;
//    mkl_rand(nb, streamptr, out);
//}
////NOTE: can do the following directly in Python
//void mkl_sobol_randn_unscrambled(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, double * restrict out)
//{
//    const unsigned int nb = m*dim;
//    mkl_randn(nb, streamptr, out);
//}

void mkl_sobol_randn(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, const unsigned int * restrict shifts, double * restrict out){
    #ifdef __INTEL_COMPILER
        __assume_aligned(out, ALIGN);
    #endif

    int status = 0;
    const unsigned int nb = m*dim;

    mkl_sobol_rand(dim, m, streamptr, shifts, out);

    vdCdfNormInv(nb, out, out);
    status = vmlGetErrStatus();
    // Hopefully this will never happen
    if((status == VML_STATUS_SING) || (status == VML_STATUS_ERRDOM) || (status == VML_STATUS_OVERFLOW))
    {
        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
        #endif
        #pragma omp simd aligned(out : ALIGN)
        for(unsigned int i = 0; i<nb; i++)
        {
            if(!isfinite(out[i])){
                out[i] = 0.0;
            }
        }
    }

}

void mkl_sobol_rand(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, const unsigned int * restrict shifts, double * restrict out){
    int status = 0;
    const unsigned int nb = m*dim;
    unsigned int i,k;

    unsigned int * restrict unscrambled __attribute__((aligned(ALIGN)));

    // Allocate
    unscrambled = (unsigned int *)mkl_malloc(nb*sizeof(unsigned int), ALIGN);
    check_allocation_successful(unscrambled);

    status = viRngUniformBits( VSL_RNG_METHOD_UNIFORMBITS_STD, *streamptr, nb, unscrambled);

    for(i = 0; i<m; i++){
        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(unscrambled, ALIGN);
            __assume_aligned(shifts, ALIGN);
        #endif
        #pragma omp simd aligned(out, unscrambled, shifts : ALIGN)
        for(k=0; k<dim; k++){
            out[i*dim + k] = ((double) (unscrambled[i*dim + k] ^ shifts[k]) * TWO_POWER_MINUS_32);
        }
    }

    mkl_free(unscrambled);
}

void mkl_sobol_rand_bits(const unsigned int dim, const unsigned int m, const VSLStreamStatePtr * streamptr, const unsigned int * restrict shifts, unsigned int * restrict out){
    int status = 0;
    const unsigned int nb = m*dim;
    unsigned int i,k;

    status = viRngUniformBits( VSL_RNG_METHOD_UNIFORMBITS_STD, *streamptr, nb, out);
    for(i = 0; i<m; i++){
        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(shifts, ALIGN);
        #endif
        #pragma omp simd aligned(out, shifts : ALIGN)
        for(k=0; k<dim; k++){
            out[i*dim + k] ^= shifts[k];
        }
    }
}

void SetupSobolStream(VSLStreamStatePtr ** streamptr, const unsigned int dim){
    if(dim == 0 || dim > MAX_DIM){
        printf("Error: dimension should be a positive number smaller or equal than %d\n", MAX_DIM);
        exit(1);
    }

    *streamptr = (VSLStreamStatePtr*) mkl_malloc(sizeof(VSLStreamStatePtr), ALIGN);
    check_allocation_successful(streamptr);

    int status = 0;

    // if dim is less or equal than 40 there is no need to supply direction numbers and generating polynomials
    // and we can just use the default Intel MKL Sobol stream */
    if(dim <= 40){
        status = vslNewStream( *streamptr, VSL_BRNG_SOBOL, dim);
        return;
    }

    // We now need to prescribe direction numbers and generating polynomials to vslNewStreamEx
    // The number of generating polynomials is = dim - 1
    const unsigned int n_sobol_poly = dim - 1;
    // dimensions at which the maximum polynomial degree increases by 1 (see Frances Kuo website)
    const unsigned int deg_positions[] = {2, 3, 4, 6, 8, 14, 20, 38, 54, 102, 162, 338, 482, 1112, 1868, 3668, 5716, 13426, 30000};

    unsigned int i,j,k;

    // compute maximum degree of the generating polynomials
    for(i = 0; (deg_positions[i] < dim) && (i < 19); i++);

    const unsigned int maxdeg = i;
    const unsigned int how_many = 4 + (dim-1)*(maxdeg+1);
    const MKL_INT how_many_mkl = (MKL_INT) how_many;

    unsigned int * restrict params __attribute__((aligned(ALIGN)));

    // Table of initial direction numbers
    const unsigned int sobol_size = n_sobol_poly*maxdeg*sizeof(unsigned int);
    unsigned int **dir_numbers;
    unsigned int * restrict dir_numbers_flat __attribute__((aligned(ALIGN)));
    // Primitive polynomials
    unsigned int * restrict primitive_polys __attribute__((aligned(ALIGN)));

    // Allocate everything needed
    params = (unsigned int *)mkl_malloc(how_many*sizeof(unsigned int), ALIGN);
    check_allocation_successful(params);

    dir_numbers_flat = (unsigned int *)mkl_malloc(sobol_size, ALIGN);
    check_allocation_successful(dir_numbers_flat);

    dir_numbers = (unsigned int **)mkl_malloc(n_sobol_poly*sizeof(unsigned int*), ALIGN);
    check_allocation_successful(dir_numbers);

    primitive_polys = (unsigned int *)mkl_malloc(n_sobol_poly*sizeof(unsigned int), ALIGN);
    check_allocation_successful(primitive_polys);

    #ifdef __INTEL_COMPILER
        __assume_aligned(params, ALIGN);
        __assume_aligned(dir_numbers_flat, ALIGN);
        __assume_aligned(primitive_polys, ALIGN);
    #endif

    for(i = 0; i < n_sobol_poly; i++)
        dir_numbers[i] = dir_numbers_flat + i*maxdeg;

    // Read sobol sequence data needed from file
    status = load_sobol_file(n_sobol_poly, maxdeg, dir_numbers, primitive_polys);

    if(status != 0){
        printf("Error: could not read Sobol data\n");
        DeallocateStreamPtr(streamptr);
        exit(1);
    }

    // Feed Sobol data into the stream
    params[0] = dim;
    params[1] = VSL_USER_QRNG_INITIAL_VALUES;
    params[2] = VSL_USER_INIT_DIRECTION_NUMBERS|VSL_USER_PRIMITIVE_POLYMS;
    for(i = 0; i < dim-1; i++) params[i+3] = primitive_polys[i];
    params[2+dim] = maxdeg;
    k = 3+dim;
    for(i = 1; i < dim; i++)
        for(j = 0; j < maxdeg; j++)
            params[k++] = dir_numbers[i-1][j];

    status = vslNewStreamEx(*streamptr, VSL_BRNG_SOBOL, how_many_mkl, params );

    mkl_free(dir_numbers_flat);
    mkl_free(dir_numbers);
    mkl_free(primitive_polys);
    // FIXME is the following safe to do?
    mkl_free(params);
}

void check_allocation_successful(void *allocated_mem){
    if (!allocated_mem){
        printf("Error: Allocation unsuccessful\n");
        exit(1);
    }
}

int load_sobol_file(int n_sobol_poly, int maxdeg, unsigned int **dir_numbers, unsigned int *primitive_polys){
    FILE *file;
    char filename[] = __SOBOLPATH__ "/mkl_sobol.dat";
    int i,j,dim,deg;

    file = fopen(filename, "r");
    if(!file){
        printf("Error: Sobol input data file could not be found or opened!\n");
        return 1;
    }

    fscanf(file, "%*[^\n]\n");

    for(i = 0; i < n_sobol_poly; i++){
        fscanf(file, "%*d");
        fscanf(file, "%d", &deg);
        fscanf(file, "%u", &primitive_polys[i]);

		// accounting for the const and highest power (Kuo does not include them).
		// for example x^3 + x + 1 is read as (01)_2 = 1 while mkl uses it as 2^3 + 2^1 + 1 = (1011)_2 = 11.
		primitive_polys[i] = (primitive_polys[i] << 1) + (1 << deg) + 1;

        for(j = 0; j < deg; j++)
            fscanf(file, "%u ", &dir_numbers[i][j]);

        fscanf(file, "\n");
    }

    fclose(file);
    return 0;
}

void compute_integral(int dim, int n, double *r, double *partial1, double *partial2 ){
    int i,j;
    double temp;
    double s1,s2;

    s1 = 0.0;
    s2 = 0.0;
    for(i=0; i < n; i++){
        temp = 1.0;
        #ifdef __INTEL_COMPILER
            __assume_aligned(r, ALIGN);
        #endif
        #pragma omp simd aligned(r : ALIGN)
        for(j = 0; j < dim; ++j){
            temp *= 1.0 + (r[i*dim + j] - 0.5) / (double)((j+1)*(j+1));
        }
        s1 += temp;
        s2 += temp*temp;
    }

    *partial1 = s1;
    *partial2 = s2;
}

