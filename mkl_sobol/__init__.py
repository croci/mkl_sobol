#!/usr/bin/env python3

#######################################################################
#
# This file is part of mkl_sobol
#
# mkl_sobol is copyright (C) 2016-2019 Matteo Croci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#######################################################################

__author__     = 'Matteo Croci'
__credits__    = ['Matteo Croci', 'Oliver Sheridan-Methven', 'Michael B. Giles']
__license__    = 'GPL-3'
__maintainer__ = 'Matteo Croci'
__email__      = 'matteo.croci@maths.ox.ac.uk'

from .mkl_sobol import MKL_RNG, MKL_SOBOL_RNG

